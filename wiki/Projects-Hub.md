# Official Plugins

### ExtractorPlugin

> This plugin can extract the info, search, and play songs directly from its source.

- [@distube/youtube](https://www.npmjs.com/package/@distube/youtube): Support YouTube.
- [@distube/soundcloud](https://www.npmjs.com/package/@distube/soundcloud): Support SoundCloud.

### InfoExtractorPlugin

> This plugin only can extract the info from supported links, but not play songs directly from its source.

- [@distube/spotify](https://www.npmjs.com/package/@distube/spotify): Support Spotify.
- [@distube/deezer](https://www.npmjs.com/package/@distube/deezer): Support Deezer.

### PlayableExtractorPlugin

> This plugin can extract and play songs from supported links, but cannot search for songs from its source.

- [@distube/yt-dlp](https://www.npmjs.com/package/@distube/yt-dlp): Use `yt-dlp` for supporting 700+ sites.
- [@distube/direct-link](https://www.npmjs.com/package/@distube/direct-link): Support direct audio links.
- [@distube/file](https://www.npmjs.com/package/@distube/file): Support local files.

# Unofficial Plugins

### InfoExtractorPlugin

- [distube-apple-music](https://www.npmjs.com/package/distube-apple-music): Support Apple Music.
- [distube-tidal](https://www.npmjs.com/package/distube-tidal): Support Tidal.

# Bots

- [DisTube Example](https://github.com/distubejs/example) - Example bot with simple command handler.
